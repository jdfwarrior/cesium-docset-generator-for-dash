#! /usr/bin/env node
var cheerio 	= require('cheerio')
var args 		= process.argv.slice(2)
var fs 			= require('fs-extra')
var async 		= require('async')
var _ 			= require('underscore')
var sqlite3 	= require('sqlite3')
var status 		= require('node-status')

var docpath 	= args[0]
var dest		= args[1]

fs.readdir(docpath,function(err,files){
	// filter out anything that isn't a file ending in .html
	files = _.filter(files,function(ele){ return /.html$/.test(ele) })

	var progress = status.addItem({
		type:'percentage',
		name:'Complete',
		count:0,
		max:files.length+6
	})
	var current = status.addItem({
		type:'text',
		name:'Status'
	})

	status.start({interval:200,label:'Time'})
	
	// create additional subdirectories
	progress.inc()
	current.text = 'Creating directory structure'
	fs.mkdirsSync(dest+'/Cesium/Contents/Resources/Documents')
	
	// copy the Info.plist into place
	progress.inc()
	current.text = 'Copying assets into project'
	fs.copySync(__dirname+'/Info.plist',dest+'/Cesium/Contents/Info.plist')
	fs.copySync(__dirname+'/icon.png',dest+'/Cesium/icon.png')
	fs.copySync(docpath+'/styles',dest+'/Cesium/Contents/Resources/Documents/styles')
	fs.copySync(docpath+'/images',dest+'/Cesium/Contents/Resources/Documents/images')
	fs.copySync(docpath+'/fonts',dest+'/Cesium/Contents/Resources/Documents/fonts')
	fs.copySync(docpath+'/icons',dest+'/Cesium/Contents/Resources/Documents/icons')
	fs.copySync(docpath+'/javascript',dest+'/Cesium/Contents/Resources/Documents/javascript')
	
	// create the sqlite database
	progress.inc()
	current.text = 'Creating database'
	var db = new sqlite3.Database(dest+'/Cesium/Contents/Resources/docSet.dsidx')
	
	// build table structure
	progress.inc()
	current.text = 'Building database structure'
	db.run('CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, path TEXT)',function(){
		// database created, creating index
		db.run('CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path)')
	})

	async.eachSeries(files,function(file,callback){
		progress.inc()
		current.text = file
		// copy the file to the Documents folder for reference
		fs.createReadStream(docpath+'/'+file).pipe(fs.createWriteStream(dest+'/Cesium/Contents/Resources/Documents/'+file))
		// read each file
		fs.readFile(docpath+'/'+file, function(err,data){
			$ = cheerio.load(data)
			var title = $('h1.page-title').text().trim()
			db.run('INSERT OR IGNORE INTO searchIndex(name, type, path) VALUES ("'+title+'", "Class", "'+file+'")')
			
			var inst  = $('.container-overview','article').find('h4').text().trim().split(/\s{3}/)
				inst  = _.first(inst)
			if (inst) db.run('INSERT OR IGNORE INTO searchIndex(name, type, path) VALUES ("'+inst+'", "Constructor", "'+file+'#'+title+'")')
				
			var subsections = $('h3.subsection-title','article')
			
			_.each(subsections,function(sub){
				var items = $(sub).next('dl').contents()
				var anchor
				_.each(items,function(item){
					anchor = $(item).find('h4').attr('id')
					item = $(item).find('h4').text().trim().split(/\s{2}/)
					if (item.length>1){
						item = _.first(item)
						if (/^readonly/.test(item)){item = item.replace('readonly','')}
						if (/^static/.test(item)){item = item.replace('static','')}
						var type 
						if ($(sub).text().trim() === 'Members') type = 'Property'
						else if ($(sub).text().trim() === 'Methods') type = 'Method'
						else if ($(sub).text().trim() === 'Type Definitions') type = 'Type'
						else type = 'Attribute'
						db.run('INSERT OR IGNORE INTO searchIndex(name, type, path) VALUES ("'+item+'", "'+type+'", "'+file+'#'+anchor+'")')
					}
				})
			})
			callback(null)
		})
	},function(){
		// close the database
		progress.inc()
		current.text = 'Closing database'
		db.close(function(){
			// rename the package
			progress.inc()
			current.text = 'Renaming package'
			fs.rename(dest+'/Cesium',dest+'/Cesium.docset')
		
			// inform the user that we're done
			current.text = 'Complete'
			status.stop()
			process.exit(1)
		})
	})	
})
